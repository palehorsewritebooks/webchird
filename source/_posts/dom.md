---
title: DOM簡介
---

## Document Object Model，簡稱為DOM
 > The Document Object Model (DOM) is a programming interface for HTML and XML documents. It represents the page so that programs can change the document structure, style, and content. The DOM represents the document as nodes and objects. That way, programming languages can connect to the page.<hr />DOM是HTML和XML文件檔案的程式化介面，程式可以去修改文件的內部結構、樣式、內容等等。DOM用__節點(node)__和__物件(object)__的形式呈現文件內容，好讓程式語言可以和死的文件檔案連結。

  *以上內容取自[MDN](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)* 
<!-- more -->
## HTML的沈睡與甦醒
   說穿了，HTML僅僅是__死的檔案__，只是一堆文字、符號、標籤組成的文件，在平時~~就像死魚一樣~~動都不動。什麼時候會甦醒過來呢？就是當你用__瀏覽器__打開了HTML檔案，瀏覽器內建的__HTML引擎__會為HTML檔案注入活力，<a\>、<p\>、<h1\>、<div\>等等__標籤__會紛紛活過來，搖身一變成為__HTML元素__，按照事先規範好的尺寸大小、顏色、粗體、斜體等等屬性，在瀏覽器上呈現，這時的HTML檔案就不僅僅是死的檔案了，而是我們所熟知的__網頁__。

   也就是說，當瀏覽器開啟了HTML檔案，HTML就會變成活蹦亂跳的網頁，當瀏覽器被關掉，HTML檔案則就歸於寂靜，恢復成僅僅是一堆文字、符號組成的靜態檔案。

## JavaScript和HTML的彆扭關係
   承上，HTML對於JavaScript來說，就是一個死氣沈沈的文件罷了，平時JavaScript對這樣的靜態檔案毫無辦法。然而，__JavaScript存在的目的就是要能夠隨時改動HTML的內容__，不管是文字、結構、樣式等等，如果做不到這一點，JavaScript簡直毫無意義。

   JavaScript什麼時候能夠擁有對__HTML內容__的控制權呢？同上，就是當瀏覽器把網頁打開，瀏覽器內建的__JavaScript引擎__能夠用__「JavaScript的方式」__掌控一切。

   但是基於某種矜持(?)，HTML也不是能夠隨隨便便就能被掌控，JavaScript必須要用對方法，為此JavaScript引擎創造了一種物件：__document__，就是專門對付HTML、XML這樣的標籤文件。__document物件__下所屬的各種方法和屬性，能夠想方設法控制HTML的種種內容。

## DOM的真正意義
   當JavaScript引擎發揮威力，例如使用了__document.getElementById()__這個方法，不只是靠著id屬性從HTML文件中取得了某個元素，JavaScript引擎還順道做了一點額外服務，__把取得的元素變成物件__，讓JavaScript的開發者能夠方便使用，如下：
```javascript
   /* 這是JavaScript從HTML文件中，取得一個id屬性叫做"latest"的元素，並且把元素順便轉化成物件 */
   var x = document.getElementById('latest');
   
   /* 轉成物件後，就能透過JavaScript程式語言把latest隱藏起來了 */
   x.style = "display: none";
   
   /* 透過JavaScript程式語言把latest拉長 */
   x.style = parseInt(x.style.width) * 2 + 'px';
```

   透過JavaScript引擎所轉化的HTML元素，就稱為__DOM__。JavaScript可以利用DOM，把HTML元素搓成長的、捏成扁的、揉成一團，請注意，在瀏覽器沒開之前，JavaScript是完全做不到的，__現在你知道DOM的真正意義何在了__。